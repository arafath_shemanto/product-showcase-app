import React from "react";
import "./Review.css";
const Review = ({ review }) => {
  const { name, desc, ratings } = review;
  return (
    <div className="col-xl-4">
      <div className="review_box">
        <h4>{name}</h4>
        <h5>Ratings: {ratings}</h5>
        <p>{desc}</p>
      </div>
    </div>
  );
};

export default Review;
