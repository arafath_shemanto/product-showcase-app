import React, { useEffect, useState } from "react";
import UseReview from "../../Hooks/UseReview";
import Review from "../Review/Review";

const Reviews = () => {
  const [reviews, setReviews] = UseReview([]);
  return (
    <div className="review_wrapper section_pading">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h3 className="theme_title text-center">Client reviews</h3>
          </div>
        </div>
        <div className="row">
          {reviews.map((review) => (
            <Review key={review.id} review={review}></Review>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Reviews;
