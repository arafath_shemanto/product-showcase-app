import React from "react";
import Barchart from "../BarChart/Barchart";
import Linechart from "../Linechart/Linechart";

const Dashboard = () => {
  return (
    <div className="section_pading">
      <div className="container">
        <div className="row">
          <div className="col-xl-6">
            <h4 className="theme_title">Line Chart</h4>
            <Linechart></Linechart>
          </div>
          <div className="col-xl-6">
            <h4 className="theme_title">Bar Chart</h4>
            <Barchart></Barchart>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
