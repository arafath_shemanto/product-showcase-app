import React from "react";
import "./Home.css";
import bike from "../../Assets/img/bike.jpg";
import Review from "../Review/Review";
import UseReview from "../../Hooks/UseReview";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [reviews, setReviews] = UseReview([]);
  const navigate = useNavigate();
  const navigateHandler = () => {
    navigate("/reviews");
  };
  return (
    <div>
      <div className="home_banner">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-xl-6">
              <div className="banner__text">
                <span>Yamaha Brand</span>
                <h3>Yamaha R15 V3 Speed is Reborn</h3>
                <p>
                  The original supersport is back! Born of Yamaha’s racing DNA
                  that shaped legends like M1, The R1 and the R-series, the
                  all-new R15 powered with Dual Channel ABS is max powerhouse of
                  18.6 PS that brings track experience to the streets.{" "}
                </p>
              </div>
            </div>
            <div className="col-xl-6">
              <div className="bike_img">
                <img src={bike} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* review section  */}
      <div className="review_wrapper section_pading">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h3 className="theme_title text-center">Client reviews(03)</h3>
            </div>
          </div>
          <div className="row">
            {reviews.slice(0, 3).map((review) => (
              <Review key={review.id} review={review}></Review>
            ))}
          </div>
          <div className="row">
            <div className="col-12 text-center">
              <button onClick={navigateHandler} className="theme_btn">
                See all reviews
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* review section::end */}
    </div>
  );
};

export default Home;
