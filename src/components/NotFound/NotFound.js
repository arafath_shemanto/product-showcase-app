import React from "react";
import "./NotFound.css";
const NotFound = () => {
  return (
    <div className="notFoundText_banner">
      <div className="container">
        <div className="row">
          <div className="col-12 text-center">
            <h3>Sorry!!! the page is not found</h3>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
