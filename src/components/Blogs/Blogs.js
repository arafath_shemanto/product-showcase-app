import React from "react";

const Blogs = () => {
  return (
    <div className="section_pading">
      <div className="container">
        <div className="row">
          <div className="col-xl-12 mb-4">
            <h3 className="theme_title mb-3 text-uppercase">Context api</h3>
            <h4>
              React এ আমরা এক কম্পোনেন্ট থেকে আর এক কম্পোনেন্ট এ ডাটা পাঠানোর
              জন্যে props ব্যাবহার করি। প্যারেন্ট থেকে চাইল্ড কম্পোনেন্ট এ ডাটা
              পাঠানোর ক্ষেত্রে অনেক সময় চাইল্ড কম্পোনেন্ট গুলো অনেক ভিতরে থাকে।
              Context api হলো React এর এটকি hook। Context api এর মাদ্ধমে আমরা
              সহজেই এই প্রপ্স ড্রিলিং না করেই ডাটা পাঠাতে পারি। Provider এর
              ভিতরে সব চাইল্ড গুলোকে রাখতে হয়। Provider এর ভিতরে থাকা সব চাইল্ড
              Provider বা প্যারেন্ট এর সব ডাটা ব্যাবহার করতে পারে।
            </h4>
          </div>
          <div className="col-xl-12">
            <h3 className="theme_title mb-3 text-uppercase">
              Block Element & Inline Element
            </h3>
            <h4>
              Block Element গুলো parent Element এর পুরো জায়গা নিয়ে নেয়। অর্থাৎ
              ব্লক Element গুলো প্যারেন্ট এর পুরো জায়গা তা দখল করে থাকে। উদাহরণ
              : h1,h2,h3,p button,
            </h4>
            <h4>
              Inline Element গুলো একটা শেষ হলে আর একটা শুরু হয়। এটা প্যারেন্ট
              এলিমেন্ট এর পুরো জায়গাটা দখল করে না। css দিয়ে Inline Block elemnt
              গুলোকে ব্লক এলিমেন্ট বানানো যায় | উদাহরণ : span, a,
            </h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blogs;
